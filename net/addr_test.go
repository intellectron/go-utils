package net

import (
	"bytes"
	"fmt"
	"net"
	"strings"
	"testing"
)

func TestGetAddrPort(t *testing.T) {
	tables := []struct {
		network string
		address string
		expect  int
	}{
		{
			"tcp",
			"1.1.1.1:http",
			80,
		},
		{
			"udp",
			"1.1.1.1:110",
			110,
		},
	}

	var port int
	for _, tc := range tables {
		switch tc.network {
		case "tcp":
			tcpAddr, err := net.ResolveTCPAddr(tc.network, tc.address)
			if err != nil {
				panic(err)
			}
			port = getAddrPort(tcpAddr)
		case "udp":
			udpAddr, err := net.ResolveUDPAddr(tc.network, tc.address)
			if err != nil {
				panic(err)
			}
			port = getAddrPort(udpAddr)
		}
		if port != tc.expect {
			t.Errorf("get port from %s is %d, but expect %d", tc.address, port, tc.expect)
		}
	}
}

func TestGetAddrIP(t *testing.T) {
	tables := []struct {
		network string
		address string
		expect  net.IP
	}{
		{
			"tcp",
			"1.1.1.1:http",
			net.ParseIP("1.1.1.1"),
		},
		{
			"udp",
			"2.2.2.2:110",
			net.ParseIP("2.2.2.2"),
		},
	}

	var ip net.IP
	for _, tc := range tables {
		switch tc.network {
		case "tcp":
			tcpAddr, err := net.ResolveTCPAddr(tc.network, tc.address)
			if err != nil {
				panic(err)
			}
			ip = getAddrIP(tcpAddr)
		case "udp":
			udpAddr, err := net.ResolveUDPAddr(tc.network, tc.address)
			if err != nil {
				panic(err)
			}
			ip = getAddrIP(udpAddr)
		}
		if !bytes.Equal(ip, tc.expect) {
			t.Errorf("get ip from %s is %s, but expect %s", tc.address, ip, tc.expect)
		}
	}
}

func TestParseHostPort(t *testing.T) {
	table := []struct {
		hostPort string
		host     string
		port     int
	}{
		{
			"www.baidu.com:9999",
			"www.baidu.com",
			9999,
		},
		{
			"1.1.1.1:8888",
			"1.1.1.1",
			8888,
		},
	}

	for _, tc := range table {
		host, port, err := ParseHostPort(tc.hostPort)
		if err != nil {
			panic(err)
		}

		if host != tc.host || port != tc.port {
			t.Errorf("expect: %s %d, get: %s %d", tc.host, tc.port, host, port)
		}
	}

}

func TestIsAddrInUse(t *testing.T) {
	firstTcpAddr, _ := net.ResolveTCPAddr("tcp", "0.0.0.0:36909")
	secondTcpAddr, _ := net.ResolveTCPAddr("tcp", "0.0.0.0:65511")
	table := []struct {
		tcpAddr *net.TCPAddr
		expect  bool
	}{
		{
			firstTcpAddr,
			true,
		},
		{
			secondTcpAddr,
			false,
		},
	}

	for _, tc := range table {
		inUse, err := IsAddrInUse(tc.tcpAddr)
		fmt.Println(inUse, err)
		if err != nil && !strings.Contains(err.Error(), "only support") {
			if inUse != tc.expect {
				t.Errorf("expect: %t, get:%t", tc.expect, inUse)
			}
		}
	}
}
