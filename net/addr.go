package net

import (
	"errors"
	"net"
	"strconv"
	"strings"
)

// Extract port as an integer from an net.Addr interface
func getAddrPort(addr net.Addr) int {
	switch rawAddr := addr.(type) {
	case *net.UDPAddr:
		return rawAddr.Port
	case *net.TCPAddr:
		return rawAddr.Port
	default:
		_, port, err := net.SplitHostPort(addr.String())
		if err != nil {
			// If no port in net.Addr, it will panic
			panic(err)
		}

		i64, err := strconv.ParseInt(port, 0, 0)
		if err != nil {
			panic(err)
		}
		return int(i64)
	}
}

// Extract ip from as net.IP from net.Addr interface
func getAddrIP(addr net.Addr) net.IP {
	switch rawAddr := addr.(type) {
	case *net.UDPAddr:
		return rawAddr.IP
	case *net.TCPAddr:
		return rawAddr.IP
	default:
		host, _, err := net.SplitHostPort(addr.String())
		if err != nil {
			panic(nil)
		}
		return net.ParseIP(host)
	}
}

// a wrapper of net.SplitHostPort which return a integer port
func ParseHostPort(hostPort string) (host string, port int, err error) {
	host, portStr, err := net.SplitHostPort(hostPort)
	if err != nil {
		return
	}

	port64, err := strconv.ParseInt(portStr, 0, 0)
	if err != nil {
		return
	}
	port = int(port64)
	return
}

// Examine addr is in use or not by check returned error
func IsAddrInUse(addr net.Addr) (inUse bool, err error) {
	switch addr.(type) {
	case *net.TCPAddr:
		_, err = net.ListenTCP("tcp", addr.(*net.TCPAddr))
		if err != nil {
			inUse = strings.Contains(err.Error(), "address already in use")
		} else {
			inUse = false
		}
	case *net.UDPAddr:
		_, err = net.ListenUDP("udp", addr.(*net.UDPAddr))
		if err != nil {
			inUse = strings.Contains(err.Error(), "address already in use")
		} else {
			inUse = false
		}
	default:
		err = errors.New("only support tcp and udp")
	}
	return
}
